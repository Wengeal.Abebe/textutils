[![pipeline status](https://git.app.uib.no/inf112/24v/textutils/badges/main/pipeline.svg)](https://git.app.uib.no/inf112/24v/textutils/-/commits/main) [![coverage report](https://git.app.uib.no/inf112/24v/textutils/badges/main/coverage.svg)](https://git.app.uib.no/inf112/24v/textutils/-/commits/main)

# INF112 TextUtils (for Øving 1)

* Se [Øving 1](https://git.app.uib.no/inf112/24v/inf112-24v/-/wikis/lab-01-intro/oving1)
